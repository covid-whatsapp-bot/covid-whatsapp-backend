"""
Backend server for bhoot's whatsapp bot for covid

Contact JSON format:

    [
        {
            "name": <Name of the Institute (string)>
            "contact": [
                {
                    "name": <Name of the contact. 'noname' if no name (string)>,
                    "number: <up to 15 digit string>,
                },
            ],
            "subcategory": [
                "subcategory1" (<string>),
                "subcategory2",
            ],
            "address": <Address (string)>,
            "map_link": <google maps link (string)>,
            "notes": <preformatted notes to be displayed (string)>,
            "priority": <Integer, with lower the number = higher priority>,
        },
        {
            ... Another such json ...
        },
    ]
"""

from flask import Flask, request
from flask_restful import Resource, Api

from arango import ArangoClient
from arango_creds import get_creds

from datetime import datetime
import pytz

IST = pytz.timezone('Asia/Kolkata')

app = Flask(__name__)
api = Api(app)

#get credentials
db_creds = get_creds()

#db init
def init_db():
    global db
    global data_collection
    global config_collection
    global feedback_collection
    global log_collection
    
    client = ArangoClient(hosts=db_creds['arango_url'])
    # Connect to "_system" database as root user.
    # This returns an API wrapper for "_system" database.
    sys_db = client.db('_system', username='root', password=db_creds['root_pwd'])

    # Create a new database named "physics" if it does not exist.
    if not sys_db.has_database(db_creds['db_name']):
        #sys_db.create_database(db_name)
        print("E:db not found")
        return
    
    db = client.db(db_creds['db_name'], username=db_creds['usr_name'], password=db_creds['usr_pwd'])
    
    #collection init
    if db.has_collection(db_creds['data_collection_name']):
        data_collection = db.collection(db_creds['data_collection_name'])
    else:
        print("E:collection not found and created: "+node_collection_name)
        #node_collection = db.create_collection(db_creds['data_collection_name'])
    if db.has_collection(db_creds['config_collection_name']):
        config_collection = db.collection(db_creds['config_collection_name'])
    else:
        print("E:collection not found and created: "+db_creds['config_collection_name'])
        #config_collection = db.create_collection(db_creds['config_collection_name'])
    if db.has_collection(db_creds['feedback_collection_name']):
        feedback_collection = db.collection(db_creds['feedback_collection_name'])
    else:
        print("E:collection not found and created: "+db_creds['feedback_collection_name'])
        #feedback_collection = db.create_collection(db_creds['feedback_collection_name'])
    if db.has_collection(db_creds['log_collection_name']):
        log_collection = db.collection(db_creds['log_collection_name'])
    else:
        print("E:collection not found and created: "+db_creds['log_collection_name'])
        #log_collection = db.create_collection(db_creds['log_collection_name'])

#TODO add try catch

#get menubylanguage
def get_menu():
    return config_collection.get('menu')['menu_data']

#get trigger messages
def get_help_triggers():
    #print(config_collection.get('config')['help_triggers'])
    return config_collection.get('triggers')['help_triggers']

def get_lang_triggers():
    #print(config_collection.get('config')['help_triggers'])
    return config_collection.get('triggers')['language_triggers']

#get contactsbysubcategory(max number of items)
def get_contacts(subcategory, limit):
    out = []
    ctr = 0
    for i in data_collection:
        if subcategory in i['subcategory']:
            out.append(i)
            ctr += 1
            if ctr >= limit:
                return out
    return out

#store request log in db
def store_log(num, sub_category):
    log_collection.insert({'usr':num, 'subcategory':sub_category, 'timestamp':datetime.now(IST).strftime("%H%M%S%d%m%Y")})

#check contact number in db

class TheBackend(Resource):
    def get(self):
        """Returns data from the spreadsheet.

        Takes arguments in the following format (GET query params or POST args)

        sub_category:<string>

        Returns: a Contact JSON (refer to top of the file for json format)
        """
        subcategory = request.args["sub"]
        number = request.args["no"]
        
        store_log(number, subcategory)
        
        return get_contacts(subcategory, 20)

class ConfigBackend(Resource):
    def get(self, item):
        """Returns config parameters

        Takes arguments in the following format (GET query params or POST args)

        item:<string>

        Returns: relevant config details
        """
        if item == 'help_triggers':
            return get_help_triggers()
        elif item == 'lang_triggers':
            return get_lang_triggers()
        elif item == 'menu':
            return get_menu()
        return []

api.add_resource(ConfigBackend, '/conf/<string:item>')
api.add_resource(TheBackend, '/contact')

if __name__ == '__main__':
    init_db()
    
    app.run(debug=True)
